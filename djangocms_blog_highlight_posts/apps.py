# Django
from django.apps import AppConfig


class DjangocmsBlogHighlightPostsConfig(AppConfig):
    name = "djangocms_blog_highlight_posts"
